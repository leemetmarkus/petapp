from behave import given, when, then
from pet_app.test.factories.user import UserFactory
 
@given('an anonymous user')
def step_impl(context):
    # Creates user with given credentials and saves it to database.
    u = UserFactory(username='foo', email='foo@example.com')
    u.set_password('bar')
    u.save()
 
@given('user submits a valid login credentials')
def step_impl(context):
    # Retrieve browser instance to locate elements
    br = context.browser
    br.get(context.base_url + '/login/')
 
    # locate elements by name, id, tag, css class, etc
    br.find_element_by_name('username').send_keys('foo')
    br.find_element_by_name('password').send_keys('bar')
    br.find_element_by_name('submit').click()

@when('user submits a valid login credentials')
def step_impl(context):
    # Retrieve browser instance to locate elements
    br = context.browser
    br.get(context.base_url + '/login/')
 
    # locate elements by name, id, tag, css class, etc
    br.find_element_by_name('username').send_keys('foo')
    br.find_element_by_name('password').send_keys('bar')
    br.find_element_by_name('submit').click()
 
 
@when('user submits an invalid login credentials')
def step_impl(context):
    br = context.browser
 
    br.get(context.base_url + '/login/')
 
    br.find_element_by_name('username').send_keys('foo')
    br.find_element_by_name('password').send_keys('bar-is-invalid')
    br.find_element_by_name('submit').click()
 
 
@then('user is redirected to {url} page')
def step_impl(context, url):
    br = context.browser
 
    # Python's built-in asserting function
    assert br.current_url.endswith(url), \
     br.current_url
 
 
@then('user is notified with login failed message')
def step_impl(context):
    br = context.browser
 
    assert br.current_url.endswith('/login/')
    assert 'Please enter a correct username and password'\
     in br.page_source, 'Error message is not visible on page'