from behave import given, when, then
from pet_app.models import Vet

@given('user set his names to {first} {second}')
def step_impl(context,first,second):
    br = context.browser

    br.find_element_by_name('first_name').send_keys("Foo")
    br.find_element_by_name('last_name').send_keys("Fooer")
    br.find_element_by_name('submit-user').click()

@when('user updates his names to empty names')
def step_impl(context):
    br = context.browser

    br.find_element_by_name('first_name').send_keys("  ")
    br.find_element_by_name('last_name').send_keys("  ")
    br.find_element_by_name('submit-user').click()

@when('user updates his email to {email}')
def step_impl(context):
    br = context.browser

    br.find_element_by_name('email').send_keys(email)
    br.find_element_by_name('submit-user').click()

@then('user email is updated to {email}')
def step_impl(context,email):
    br = context.browser

    new_email = br.find_element_by_name('email').text
    assert new_email==email,"Email did not update"

@then('user names should still be {first} {second}')
def step_impl(context,first,second):
    br = context.browser

    new_first_name = br.find_element_by_name('first_name').text
    new_last_name = br.find_element_by_name('last_name').text
    assert new_first_name == first,"First name was changed"
    assert new_last_name == second, "Last name was changed"