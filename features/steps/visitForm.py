from behave import given, when, then
from pet_app.models import Vet
from selenium import webdriver


@given("Three veterinarians are given")
def step_impl(context):
    vetName1 = "Peeter Oja"
    context.vetName1 = vetName1
    vetName2 = "Ott Sepp"
    vetName3 = "Markus Leemet"
    context.dogName = "kutsu"
    firstVet = Vet(name=vetName1, age=32, education="Uni")
    firstVet.save()
    seconVet = Vet(name=vetName2, age=42, education="Highschool")
    seconVet.save()
    thirdVet = Vet(name=vetName3, age=52, education="Kindergarden")
    thirdVet.save()


@when('navigate to "vets" window')
def step_impl(context):
    br = context.browser
    br.get(context.base_url + "/vets")
    bookButton = br.find_element_by_name("book")
    bookButton.click()


@when('user adds a pet with a valid name')
def step_impl(context):
    br = context.browser

    br.find_element_by_name('name').send_keys(context.dogName)
    br.find_element_by_name('submit-pet').click()


@then("Veterinarian name should be first veterinarian name")
def step_impl(context):
    br = context.browser
    actualName = br.find_element_by_name("vet").text
    assert actualName == context.vetName1


@when('user goes to the profile page')
def step_impl(context):
    br = context.browser
    br.get(context.base_url + '/profile')
 
