from behave import given, when, then
from pet_app.test.factories.user import UserFactory

@given('user goes to the profile page')
def step_impl(context):
    br = context.browser
    br.get(context.base_url + '/profile')
 