from behave import given, when, then
from pet_app.test.factories.user import UserFactory
from pet_app.models import Pet

@when('user adds a pet with a name of space')
def step_impl(context):
    br = context.browser

    br.find_element_by_name('name').send_keys(' ')
    br.find_element_by_name('submit-pet').click()




@then('pet with a name of space is displayed on the screen')
def step_impl(context):
    a = len(list(Pet.objects.filter(name=' ')))
    assert (a==0),"No pet was found in database"