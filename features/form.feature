# Created by MarkusLeemet at 08.04.2019
Feature: Registering to veterinarian with pet
  All scenarios are related to registering form

  Scenario: Autocomplete veterinarian name
    Given Three veterinarians are given
    And an anonymous user
    When user submits a valid login credentials
    And  user goes to the profile page
    And user adds a pet with a valid name
    And navigate to "vets" window
    Then Veterinarian name should be first veterinarian name
