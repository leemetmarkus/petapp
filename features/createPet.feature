Feature: Creating pet
 
  Scenario: Create pet with empty name
    Given an anonymous user
    And user submits a valid login credentials
    And user goes to the profile page
    When user adds a pet with a name of space
    Then pet with a name of space is displayed on the screen
