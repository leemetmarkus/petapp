Feature: Updating profile info
 
  Scenario: Update profile email
    Given an anonymous user
    And user submits a valid login credentials
    And user goes to the profile page
    When user updates his email to haha@gmail.com
    Then user email is updated to haha@gmail.com

  Scenario: Update name and last name to empty names
    Given an anonymous user
    And user submits a valid login credentials
    And user goes to the profile page
    And user set his names to Foo Fooer
    When user updates his names to empty names
    Then user names should still be Foo Fooer