Feature: Logging in
 
  Scenario: Login with valid credentials
 
    Given an anonymous user
    When user submits a valid login credentials
    Then user is redirected to / page
 
  Scenario: Login with invalid credentials
 
    Given an anonymous user
    When user submits an invalid login credentials
    Then user is notified with login failed message